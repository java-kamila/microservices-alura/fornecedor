# API Fornecedor (Floricultura)

## Estrutura

<img src="spring-micro-servicos-alura.png" alt="Estrutura dos Microservices">

 - Teremos três microsserviços: Fornecedor, Loja e Transportador

 - Uma apresentação da modelagem focado em DDD (Domain Driven Design)
     - Quebraremos o domínio em contextos menores (bounded context)
     - Um microsserviço é a implementação de um contexto

### Endpoints

<img src="endpoints_services.png" alt="Estrutura dos Microservices">

### Comunicação

<img src="eureka.png" alt="Estrutura dos Microservices">

### Anotações

 - A dependência `Eureka DIscovery Client` permite a inscrição da API como um cliente em um servidor Eureka
 - As configurações em `application.properties` 
 	- Registrando a API como cliente Eureka:
 
 ```
	eureka.client.register-with-eureka=true
	eureka.client.fetch-registry=true
	## IP do servidor Eureka
	eureka.client.service-url.defaultZone=http://localhost:8761/eureka
 ```
 	- Registrando o nome da API:
 
	 ```
	 spring:
	  application:
	    name: 'fornecedor'
	```
#### Spring Config Server

<img src="spring-config-server.png">

 - Adicionar dependência `Config Client`
 - Acessando Spring Configuration Server através da requisição `{config-uri}/{application-name}/profile`, resultando em: `http://localhost:8888/fornecedor/default`. Application.yml de Fornecedor Server:
```
spring:
  application:
    name: 'fornecedor'
  profiles:
    active: default
  cloud:
    config:
      uri: http://localhost:8888
```
 - Colocando acesso às configurações em um momento anterior ao start da aplicação. Para que o Hibernate, por exemplo, faça chamadas ao banco de dados antes das configurações serem retornadas do Configuration Server.

##### Requisições

 - GET:
 	 - http://localhost:8081/info/DF
 	 - http://localhost:8081/produto/DF
 - POST:
 	 - http://localhost:8081/pedido
 	 ```json
 	 // body
 	 [
	    {
	        "id": 3,
	        "quantidade": 6
	    },
	    {
	        "id": 1,
	        "quantidade": 15
	    }
	]
 	 ```
	