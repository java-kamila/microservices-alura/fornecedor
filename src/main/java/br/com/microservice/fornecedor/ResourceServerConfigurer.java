package br.com.microservice.fornecedor;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
public class ResourceServerConfigurer extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		/* 
		// Sinaliza que para acessar todas as requisições deve estar autenticado
		http.authorizeRequests()
			.anyRequest()
			.authenticated(); 
		*/
		
		http.authorizeRequests()
			.antMatchers(HttpMethod.POST, "/pedido") // Sinaliza que para esta requisição é necessária a Role "USER"
			.hasRole("USER");
	}
	
}
